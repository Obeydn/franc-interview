from flask import Flask, redirect, url_for, render_template, jsonify, Response, request
from datetime import datetime
import json

app = Flask(__name__)


@app.route('/')
def index():
    return redirect(url_for('login'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    with open('./users.json', 'r') as f:
        users = f.read()
    if request.method == 'POST':
        if request.form['username'] not in users or request.form['password'] != 'admin01':
            error = 'Invalid login details. Please enter valid details and try again.'
        else:
            return redirect(url_for('index_view'))
    return render_template('login.html', error=error)


@app.route('/register', methods=['GET', 'POST'])
def register():
    error = None
    with open('./users.json', 'r') as f:
        users = f.read()
    if request.method == 'POST':
        if request.form['newuser'] in users:
            error = 'Username already taken. Please enter another username and try again.'
        else:
            with open('./users.json', 'r+') as f:
                user_json = json.load(f)
                user_json[request.form['newuser']] = ["Thomas", "Richard", "Kyle"]
                f.seek(0)
                f.write(json.dumps(user_json))
                f.truncate()
            with open('./posts.json', 'r+') as f:
                post_json = json.load(f)
                user_json[request.form['newuser']] = []
                f.seek(0)
                f.write(json.dumps(user_json))
                f.truncate()
        return redirect(url_for('login'))
    return render_template('register.html', error=error)


@app.route('/home')
def index_view():
    username = request.args.get('username')
    return render_template('index.html', username=username)


@app.route('/users')
def users_view():
    with open('./users.json', 'r') as f:
        users = f.read()
    return Response(users, mimetype="application/json")


@app.route('/posts')
def posts_view():
    with open('./posts.json', 'r') as f:
        posts = f.read()
    return Response(posts, mimetype="application/json")


@app.route('/timeline/<username>')
def user_timeline_view(username):
    """
        Returns a view of the user's timeline.
    """
    with open('./posts.json', 'r') as f:
        user_data = json.loads(f.read())
    return render_template('timelines.html', user_data={
        'posts': sort_timeline(user_data, username),
        'username': username,
        'number_posts': len(user_data[username])
    })


def sort_timeline(data, username):
    """
            This function returns a sorted timeline, according to time.
    """
    return sorted(data[username], key=lambda x: datetime.strptime(x['time'], '%Y-%m-%dT%H:%M:%SZ'), reverse=False)


if __name__ == '__main__':
    app.run(debug=True)
